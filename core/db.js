'use strict';

var sqldb = require("mssql");
var settings = require("../settings");

exports.executesql = function ( sql, callback){
    var conn = new sqldb.ConnectionPool(settings.dbconfig);

    conn.connect()
        .then ( function (){
            var req = new sqldb.Request( conn );
            req.query(sql)
                .then(function (recordset){
                    callback(recordset);
                })
                .catch(function(err){
                    console.log(err);
                    callback(null, err);
                })
        })
        .catch(function(err){
            console.log(err);
            callback(null, err);
        });
};

