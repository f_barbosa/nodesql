'use strict';

var http = require("http");
var cat = require ("../controllers/categories");
var settings = require("../settings");
var port = 8080;

http.createServer ( function ( req, resp){
    switch ( req.method){
        case "GET":
            if (req.url == "/"){
                resp.end();
            }
            else if (req.url == "/categories"){
                cat.getList(req, resp );
            }
            break;
        case "POST":
            break;
        case "PUT":
            break;
        case "DELETE":
            break;
    }
}).listen(port);